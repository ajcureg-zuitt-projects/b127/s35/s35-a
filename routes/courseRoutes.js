const express = require('express');
const router = express.Router();
const courseController = require('../controllers/courseControllers');
const auth = require('../auth');

// Route for creating a course
// localhost:4000/courses
router.post('/', auth.verify, (req, res) => {
	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	courseController.addCourse(data).then(result => res.send(result));
})


// Retrieve all courses
router.get('/all', (req, res) => {
	courseController.getAllCourses().then(result => res.send(result));
})


// Retrieve all ACTIVE courses
router.get('/', (req, res) => {
	courseController.getAllActive().then(result => res.send(result));
})


// Retrieve SPECIFIC course
// router.get('/course', (req, res) => {
// 	courseController.getOneCourse().then(result => res.send(result));
// })

router.get('/:courseId', (req, res) => {
	courseController.specificCourse(req.params).then(result => res.send(result));
})


// Update a course
router.put('/:courseId',auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		courses: req.body,
		Id: req.params
	}

	courseController.updateCourse(req.params, req.body, data).then(result => res.send(result));
})

module.exports = router;
