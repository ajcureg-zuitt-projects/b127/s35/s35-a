const Course = require('../models/Course');

// Creation of Course
/*
Steps:
1. Create a conditional statement that will check if the user is an admin.
2. Create a new Course object using the Mongoose model and the information from the request body and the id from the header
3. Save the new Course to the database
*/

module.exports.addCourse = (data) => {
	// User is admin
	if(data.isAdmin){
		let newCourse = new Course({
			name: data.course.name,
			description: data.course.description,
			price: data.course.price
		})

		return newCourse.save().then((course, error) => {
			// Course creation failed
			if(error) {
				return false;
			}else{
				return true;
			}
		})
	}else{
		// User is not an admin
		return false;
	}
}

// Retrieve all courses
module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result;
	})
}

// Retrieve all ACTIVE courses
module.exports.getAllActive = () => {
	return Course.find({isActive : true}).then(result => {
		return result;
	})
}

// Retrieve SPECIFIC course
// module.exports.specificCourse = (courseId) => {
// 	return Course.findById({_id:courseId}).then(result => {
// 		return result;
// 	})
// }

module.exports.specificCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		return result;
	})
}

// Update a course

module.exports.updateCourse = (reqParams, reqBody) => {
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};

	//findyByIdandUpdate(ID, updatesToBeApplied)
	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {
		//Course is not updated
		if(error){
			return false;
		}else {
			return true;
		}
	})
}
